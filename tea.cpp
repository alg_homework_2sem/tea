/*
 * Задача 4. Чай
 * В одном из отделов крупной организации работает n человек. Как практически все сотрудники этой организации,
 * они любят пить чай в перерывах между работой. При этом они достаточно дисциплинированы и делают в день ровно
 * один перерыв, во время которого пьют чай. Для того, чтобы этот перерыв был максимально приятным, каждый из 
 * сотрудников этого отдела обязательно пьет чай одного из своих любимых сортов. В разные дни сотрудник может
 * пить чай разных сортов. Для удобства пронумеруем сорта чая числами от 1 до m.
 * Недавно сотрудники отдела купили себе большой набор чайных пакетиков, который содержит a1 пакетиков чая сорта
 * номер 1, a2 пакетиков чая сорта номер 2, ..., am пакетиков чая сорта номер m. Теперь они хотят знать,
 * на какое максимальное число дней им может хватить купленного набора так, чтобы в каждый из дней каждому из 
 * сотрудников доставался пакетик чая одного из его любимых сортов.
 * Каждый сотрудник отдела пьет в день ровно одну чашку чая, которую заваривает из одного пакетика. 
 * При этом пакетики чая не завариваются повторно.
 */
#include <iostream>
#include <queue>
#include <set>
#include <vector>
using std::pair;
using std::queue;
using std::vector;

class DinicMaxFlow {
private:
    vector<vector<long long>> capaticity, flow;
    int source, target;
    long long INF;
    int verticesCount;
    vector<int> distances;
    vector<int> lastDeleted;
    long long dfs(int vertex, long long pushFlow);
    int bfs();

public:
    DinicMaxFlow(int verticesCount, int maxCap, int source, int target);
    void addEdge(int start, int finish, long long cap); // if cap = -1 -> cap = inf
    long long findMaxFlow();
};

int getMaxDays(DinicMaxFlow &maxflow, int peopleCount);

int main() {
    int peopleCount, teaCount;
    std::cin >> peopleCount >> teaCount;
    vector<int> teaTypes;
    for (int i = 0; i < teaCount; i++) {
        int count;
        std::cin >> count;
        teaTypes.push_back(count);
    } // Вершины - 0 - исток, 1 + peopleCount + teaCount - сток, 1..peopleCount - люди, peopleCount + 1 .. peopleCount + teaCount - чайные вершины. 1e8 - maxCapaticity
    DinicMaxFlow maxflow(2 + peopleCount + teaCount, 1e8, 0, 1 + peopleCount + teaCount);
    for (int i = 0; i < peopleCount; i++) {
        int count;
        std::cin >> count;
        for (int j = 0; j < count; j++) {
            int teaType;
            std::cin >> teaType;
            maxflow.addEdge(i + 1, peopleCount + teaType, -1); // Между чаем и людьми - бесконечные ребра
        }
    }
    for (int i = 0; i < teaCount; i++) {
        maxflow.addEdge(peopleCount + i + 1, peopleCount + teaCount + 1, teaTypes[i]); // Между чаем и стоком - не больше чем кол-во чая
    }
    std::cout << getMaxDays(maxflow, peopleCount);
}

int getMaxDays(DinicMaxFlow &maxflow, int peopleCount) {
    long long left_edge = 0, right_edge = 1e8; // Больше чем 50 * 1e6 быть не может, но возьмем с запасом
    while (right_edge - left_edge > 1) {
        long long middle = (right_edge + left_edge) / 2;
        for (int i = 0; i < peopleCount; i++) { // Добавим ребра между людьми и истоком
            maxflow.addEdge(0, 1 + i, middle);
        }
        int mFlow = maxflow.findMaxFlow();
        if (mFlow >= middle * peopleCount) //  Обновим границы 
            left_edge = middle;
        else
            right_edge = middle;
    }
    return left_edge;
}
DinicMaxFlow::DinicMaxFlow(int verticesCount, int maxCap, int source,
                           int target)
        : capaticity(verticesCount, vector<long long>(verticesCount)),
          flow(verticesCount, vector<long long>(verticesCount)),
          source(source),
          target(target),
          INF(maxCap + 1),
          verticesCount(verticesCount),
          distances(verticesCount),
          lastDeleted(verticesCount) {}

void DinicMaxFlow::addEdge(int start, int finish, long long cap) {
    if (cap == -1)
        capaticity[start][finish] = INF;
    else
        capaticity[start][finish] = cap;
}

long long DinicMaxFlow::dfs(int vertex, long long pushFlow) {
    if (vertex == target || pushFlow == 0) return pushFlow;
    for (; lastDeleted[vertex] < verticesCount; lastDeleted[vertex]++) { // Будем выбрасывать ребра вдоль которых не получится добратся до стока
        int neigh = lastDeleted[vertex];
        if (distances[neigh] == distances[vertex] + 1) {
            long long pushed = dfs(neigh, std::min(1LL * pushFlow, capaticity[vertex][neigh] - flow[vertex][neigh]));
            if (pushed) {
                flow[vertex][neigh] += pushed;
                flow[neigh][vertex] += -pushed;
                return pushed;
            }
        }
    }
    return 0;
}

int DinicMaxFlow::bfs() {
    distances.assign(verticesCount, -1);
    queue<int> curQueue;
    curQueue.push(source);
    distances[source] = 0;
    while (!curQueue.empty()) {
        int curVertex = curQueue.front();
        curQueue.pop();
        for (int neigh = 0; neigh < verticesCount; neigh++) {
            if (distances[neigh] == -1 &&
                flow[curVertex][neigh] < capaticity[curVertex][neigh]) {
                curQueue.push(neigh);
                distances[neigh] = distances[curVertex] + 1;
            }
        }
    }
    return distances[target];
}

long long DinicMaxFlow::findMaxFlow() {
    long long maxflow = 0;
    for (int i = 0; i < flow.size(); i++)
        for (int j = 0; j < verticesCount; j++)
            flow[i][j] = 0;
    while (1) {
        lastDeleted.assign(verticesCount, 0);
        if (bfs() == -1) break; // Не найден путь в слоистой сети
        while (long long pushed = dfs(source, INF))
            maxflow += pushed;
    }
    return maxflow;
}

